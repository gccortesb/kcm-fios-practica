//
//  KCMDataLoader.h
//  HackerBook
//
//  Created by GLORIA C CORTES B on 9/07/15.
//  Copyright (c) 2015 KeepCodingMaster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KCMDataLoader : NSObject
@property   (nonatomic,strong) NSString *urlString;
@property (nonatomic,strong) NSData *data;

-(id) initWithUrlString: (NSString *) urlString;
-(void) loadData;
@end
