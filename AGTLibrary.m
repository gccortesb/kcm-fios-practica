//
//  AGTLibrary.m
//  HackerBook
//
//  Created by Gloria Cortés on 27/03/15.
//  Copyright (c) 2015 KeepCodingMaster. All rights reserved.
//

#import "AGTLibrary.h"
#import "AGTLibrary.h"


@implementation AGTLibrary


#pragma mark -Incializadores
-(id) initWithData:(NSData *) data {
    if(self =[super init]) {
        _books = [NSMutableArray new];
        _listsByTag = [NSMutableDictionary new];
        _orderedTags = [NSArray new];
        [self cargarLibrosWithData:data];
        _favorites = [[NSMutableArray alloc] initWithCapacity:[self.books count]];

        
    }
    return self;
}

#pragma mark - Consulta
-(NSUInteger)booksCount{
    return[self.books count];
}
-(NSUInteger)bookCountFavorites{
    return [self.favorites count];
}
-(NSUInteger)bookCountForTag: (NSString *) tag {
    return [[self.listsByTag objectForKey:tag] count];
}
-(AGTBook *) bookAtIndex: (NSUInteger) indice {
    return [self.books objectAtIndex:indice]   ;
}
-(AGTBook *) favoriteAIndex: (NSUInteger) indice {
    return [self.favorites objectAtIndex:indice];
}
-(AGTBook *) bookForTag: (NSString *) tag atIndex: (NSUInteger) indice {
    
    
    return [[self.listsByTag objectForKey:tag] objectAtIndex:indice];
}

-(NSArray *) tags {
    return self.orderedTags;
}

-(NSArray *) booksForTag:(NSString *) tag{
    return [self.listsByTag objectForKey:tag];
}

#pragma mark - Modificadores

-(void)changeFavoriteBooK:(AGTBook*) book {
    
    //Se ha cambiado el estado de favorito del libro
    // por lo tanto hay que sacarlo o incluirlo en la
    //lista de favoritos
    
    if (book.favorite){
        [self.favorites addObject:book];
        
    } else {
        [self.favorites removeObject:book];
    }
}

#pragma mark -Utils
// Carga los libros desde un archivo json

-(void) cargarLibrosWithData: (NSData *)data {
    
    if( data != nil) {
        NSError *error = nil;
        NSArray * JSONObects = [NSJSONSerialization JSONObjectWithData:data
                                                               options:kNilOptions
                                                                 error:&error];
        if (JSONObects !=nil) {
            for (NSDictionary *dict in JSONObects){
                AGTBook *book = [[AGTBook alloc] initWithDictionary:dict];
                [self insertBook:book];
            }
        }
    }
    
    // Se extraen los tags y se ordenan
    self.orderedTags = [[self.listsByTag allKeys] sortedArrayUsingSelector:@selector(compare:)];

    
    //Prueba todos los metodos
    
    for (int i = 0; i <self.books.count;i++) {
        
        NSLog(@"Indice: %d Titulo: %@", i, [[self.books objectAtIndex:i] title]);
        
    }
    
    NSArray *tags = [self tags];
    NSArray *booksByTag = nil;
    
    for (NSString *t in tags) {
        
        NSLog(@"=====TAG:: %@ =========", t);
        
        booksByTag = [self booksForTag:t];
        NSLog (@"bookCountForTag: %lu", (unsigned long)[self bookCountForTag:t]);
        
        for (int i = 0; i < booksByTag.count;i++) {
            
            NSLog(@"Indice: %d Titulo: %@", i, [[booksByTag objectAtIndex:i] title]);
            
            
        }
        
        
    }
    
    NSLog (@"bookCount: %lu", (unsigned long)self.booksCount);
    NSLog (@"bookCountFavorites: %lu", (unsigned long)self.bookCountFavorites);

  
    
}

-(void) insertBook: (AGTBook *) book {
    //Insertar el libro en el listado general de libros
    
    
    [self.books addObject:book];

    
    //Insertar por tag en cada tag, si el tag ya existe simplemente se adiciona
    //sino se agrega el tag y el libro
    for (NSString *tag in book.tags) {
        if (![[self.listsByTag allKeys] containsObject:tag]) {
            
            [self.listsByTag setValue:[NSMutableArray new] forKey:tag];
        }
        [[self.listsByTag objectForKey:tag] addObject:book];
        
        
    }

    
}
    


@end
