//
//  AGTSimplePDFViewController.m
//  HackerBook
//
//  Created by gloria on 18/04/15.
//  Copyright (c) 2015 KeepCodingMaster. All rights reserved.
//

#import "AGTSimplePDFViewController.h"
#import "AGTLibraryTableViewController.h"
#import "KCMDataLoader.h"



@implementation AGTSimplePDFViewController


-(id) initWithModel: (AGTBook *) model {
     if (self = [super initWithNibName:nil
                                bundle:nil]) {
         _model = model;
         self.title = @"PDF";
     }
    return self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL) animated {
    [super viewWillAppear:animated];
    
    // Suscribirse a la notificacion de cambio de libro en la tabla
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    [nc addObserver:self
           selector:@selector(notifyThatBookDidChange:)
               name:BOOK_DID_CHANGE_NOT
             object:nil];
    
    
    
    // Asegurarse de que no se ocupa toda la pantalla
    // cuando está en un combinador
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    //sincronizar modelo con la vista
    [self syncWithModel];
    
    
    
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //dessuscribirse de la notificacion
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -Notificaciones
    //BOOK_DID_CHANGE_NOT

-(void) notifyThatBookDidChange:(NSNotification *) notification {
    //Obtener el libro
    
    AGTBook *book = [notification.userInfo objectForKey:BOOK_NOT_KEY];
    
    //Actualizar modelo
    self.model = book;
    
    //Sincronizar modelo con vista
    [self syncWithModel];
    
}

#pragma mark -Utils

-(void) syncWithModel {
   
    
    KCMDataLoader *dataLoader = [[KCMDataLoader alloc] initWithUrlString:self.model.urlPdf.absoluteString];
    [dataLoader loadData];
    [self.pdfView loadData:dataLoader.data
                  MIMEType:@"application/pdf"
          textEncodingName:@"UTF-8"
                   baseURL:nil];
}

@end
