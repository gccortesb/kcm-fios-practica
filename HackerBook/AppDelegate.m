//
//  AppDelegate.m
//  HackerBook
//
//  Created by Gloria Cortés on 24/03/15.
//  Copyright (c) 2015 KeepCodingMaster. All rights reserved.
//

#import "AppDelegate.h"
#import "AGTBook.h"
#import "AGTBookViewController.h"
#import "AGTLibrary.h"
#import "AGTLibraryTableViewController.h"
#import "Settings.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Valores por defecto inciales
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    

    if (![def objectForKey:LAST_SELECTED_BOOK]) {
        // Se guarda el valor por defecto el primer elemento del primer tema
        [def setObject:@[@1,@0]
                forKey:LAST_SELECTED_BOOK];
        //Guardar
        [def synchronize];
    }
    NSData *datosJSON = nil;
    
    if (![def objectForKey:DATA_LOCAL_URL]) {
        
        // Se carga el archivo de datos del JSON y se guarda en documents
        datosJSON  = [self cargarJSON];
        
        //Se agrega entrada al diccionario para indicar que ya esta el archivo
        [def setObject:@"YES"
                forKey:DATA_LOCAL_URL];
        
    } else {
        
        NSError *error = nil;
        
        datosJSON = [NSData dataWithContentsOfURL:[self dataLocalURL] options:kNilOptions error:&error];
        
        
    }
    
    //Creamos una vista del tipo UIWindow
    [self setWindow:[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]]];
    
    // Se crea el modelo
    
    AGTLibrary *library = [[AGTLibrary alloc] initWithData:datosJSON];
    
    //Detectar el tipo de pantalla
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        [self configureForPadWithModel:library];
        
    } else {
        [self configureForPhoneWithModel:library];
    }
                           


    [[self window] makeKeyAndVisible];
    
  
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark - Utils

-(NSData *) cargarJSON{
    
    NSURL *JSONUrl =[NSURL URLWithString:@"https://t.co/K9ziV0z3SJ"];
    
    NSData *data =[[NSData alloc] initWithContentsOfURL:JSONUrl];
    NSURL*url = [self dataLocalURL];
    NSError *error = nil;

    BOOL rc = [data writeToURL:url options:kNilOptions error:&error];
    
    if (rc == NO) {
        //Error al guardar
        NSLog (@"Error al guardar: %@", error.userInfo);
    }
    return data;
    
    
}

- (NSURL *) dataLocalURL {
    
    // Obtener la instancia del FileManager
    
    NSFileManager *fm = [NSFileManager defaultManager];
    
    //Obtener URL del directorio de documentos
    NSArray *urls = [fm URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    NSURL*url = [urls lastObject];
    url = [url URLByAppendingPathComponent:DATA_FILE_NAME];
    return url;
    
}

-(AGTBook *) lastBookSelectedModel: (AGTLibrary *)  model{
    

    
    //Obtener el NSUseDefault
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    
    //Se sacan los valores de las coordenadas
    NSArray *coords = [def objectForKey:LAST_SELECTED_BOOK];
    NSUInteger section = [[coords objectAtIndex:0] integerValue];
    NSUInteger index = [[coords objectAtIndex:1] integerValue];
    

    
    //Obtener los tags para saber el nombre del tag
    NSArray *tagsList = model.tags;
    
    NSString *tag = [tagsList objectAtIndex:section - 1];
    
    
    
    //Obtengo el libro del tag y lo devuelvo
    return [model bookForTag:tag atIndex:index];
    
}

-(void) configureForPadWithModel:(AGTLibrary *) library{
    
    //Se crean los controladores principales
    
    AGTLibraryTableViewController *libraryView = [[AGTLibraryTableViewController alloc] initWithModel:library];
    
    AGTBookViewController *bookView =[[AGTBookViewController alloc] initWithModel:[self lastBookSelectedModel: library]];
    
    //Se crean los controladores de navegación
    
    UINavigationController *libraryNC = [[UINavigationController alloc] initWithRootViewController:libraryView];
    UINavigationController * bookNC = [[UINavigationController alloc] initWithRootViewController:bookView];
    // Se crea el combinador que contendra los dos NC
    
    UISplitViewController *splitVC = [[UISplitViewController alloc] init];
    splitVC.viewControllers = @[libraryNC,bookNC];
    
    
    // Se asignan delegados
    
    splitVC.delegate = bookView;
    libraryView.delegate = bookView;
    
    //
    self.window.rootViewController =splitVC;
    
}

-(void)  configureForPhoneWithModel:(AGTLibrary *)library {
    //Se crean los controladores principales
    
    AGTLibraryTableViewController *libraryView = [[AGTLibraryTableViewController alloc] initWithModel:library];
    
    UINavigationController *libraryNC = [[UINavigationController alloc] initWithRootViewController:libraryView];
    
    //Se asignan delegados
    libraryView.delegate = libraryView;
    
    //Se coloca como root
    
    self.window.rootViewController =libraryNC;

    
}

@end
