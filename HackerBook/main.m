//
//  main.m
//  HackerBook
//
//  Created by Gloria Cortés on 24/03/15.
//  Copyright (c) 2015 KeepCodingMaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
