//
//  Settings.h
//  HackerBook
//
//  Created by gloria on 19/04/15.
//  Copyright (c) 2015 KeepCodingMaster. All rights reserved.
//

#define LAST_SELECTED_BOOK @"lastBook"
#define DATA_LOCAL_URL @"dataLocalURL"
#define DATA_FILE_NAME @"booksData"
