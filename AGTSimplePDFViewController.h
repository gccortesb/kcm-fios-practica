//
//  AGTSimplePDFViewController.h
//  HackerBook
//
//  Created by gloria on 18/04/15.
//  Copyright (c) 2015 KeepCodingMaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGTBook.h"


@interface AGTSimplePDFViewController : UIViewController

@property (nonatomic,weak) IBOutlet UIWebView *pdfView;
@property (nonatomic,strong) AGTBook *model;

-(id) initWithModel: (AGTBook *) model;

@end
