//
//  TableViewController.h
//  HackerBook
//
//  Created by Gloria Cortés on 28/03/15.
//  Copyright (c) 2015 KeepCodingMaster. All rights reserved.
//
#define FAVORITE_SECTION 0

#define BOOK_DID_CHANGE_NOT @"bookDidChange"
#define BOOK_NOT_KEY @"bookNotKey"


#import <UIKit/UIKit.h>
@class AGTLibrary;
@class AGTBook;


@class AGTLibraryTableViewController;

@protocol AGTLibraryTableViewControllerDelegate <NSObject>
@optional
-(void) libraryTableViewController:(AGTLibraryTableViewController *) lVC
                     didSelectBook:(AGTBook *) book;

@end


@interface AGTLibraryTableViewController : UITableViewController <AGTLibraryTableViewControllerDelegate>

@property (nonatomic,strong) AGTLibrary *libraryModel;
@property (weak,nonatomic) id <AGTLibraryTableViewControllerDelegate> delegate;

-(id) initWithModel: (AGTLibrary *) libModel;


@end
