//
//  AGTBook.h
//  HackerBooks
//
//  Created by Gloria Cortés on 23/03/15.
//  Copyright (c) 2015 Gloria Cortés. All rights reserved.
//

@import Foundation;
@import UIKit;

#define BOOK_DID_CHANGE_FAVORITE_STATE @"favoriteDidChangeNotification"
#define BOOK_KEY @"BOOK_KEY"

@interface AGTBook : NSObject

@property (nonatomic,copy) NSString *title;
@property (nonatomic,strong) NSArray *authors;
@property (nonatomic,strong) NSArray *tags;
@property (nonatomic, strong) NSURL *urlImage;
@property (nonatomic, strong) NSURL *urlPdf;
@property (nonatomic) BOOL favorite;


//Designed initializer

- (id) initWithTitle: (NSString *) aTitle
             authors: (NSArray *)listAuthors
                tags: (NSArray *) listTags
          anUrlImage: (NSURL *) imageURL
            anPDFUrl:(NSURL *) pdfURL;

-(id) initWithDictionary: (NSDictionary *) dict;

//Métodos sobreescritos
-(void) setFavorite: (BOOL) newValue;

//Helpers

-(UIImage *) bookImage ;
@end
