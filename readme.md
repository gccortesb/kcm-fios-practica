#Fundamentos de IOS - Práctica Gloria Cortés


##Pregunta 1: ¿Qué otros métodos similares a isKindofClass hay? ¿En qué se distingue isMemberOfClass: ?

La primera dice si un objeto es de una clase o de alguna de las clases heredadas por esta última.

La segunda segunda dice  si es clase solo de la clase con la que se compara.

##Pregunta 2: ¿Dónde se guardarían los datos de portadas e imágenes?

En la carpeta documents del Sandbox para que no se tengan que volver a cargar en el siguiente uso de la aplicación.

##Pregunta 3: ¿Cómo reflejar en la tabla de libros cuando se selecciona/deselecciona un libro como favorito?

A través de uso de notificaciones. El libro genera una noificación de cambio de valor en su atributo favorito. Esto lo hago en el método set del atributo. La tabla está suscrita a esta notificación y hace las manipulaciones pertinentes.

##Pregunta 4 : para que la tabla se actualice, usa el método reloadData de UITableView . Esto hace que la tabla vuelva a pedir datos a su dataSource. ¿Es esto una aberración desde el punto de rendimiento (volver a cargar datos que en su mayoría ya estaban correctos)? Explica por qué no es así. ¿Hay una forma alternativa? ¿Cuando crees que vale la pena usarlo?

No porque la tabla es “inteligente” y solo recarga los datos que se están viendo.

##Pregunta 5: Cuando el usuario cambia en la tabla el libro seleccionado, el AGTSimplePDFViewController debe de actualizarse. ¿Cómo lo harías?

Usando una notificación. La notificación la lanza la tabla de libros cuando el usuario selecciona una fila. El controlador del libro se suscribe a la notificación y responde cuando se cambia el libro, cambiando el modelo y el pdf que se está mostrando.

#Julio 9 de 2015 - Respuestas a observaciones de AGBO

-La descarga de imágenes y pdf se realiza siempre a url no-local y no se almacena en caché/documents para acceso posterior en local.

Se guardan los pdfs e imagenes. Para esto se creo una clase utilitaria que permite cargar datos desde una URL. Esta clase se usa al instanciar el libro para cargar la imagen, y en el view del pdf para que cuando soliciten un pdf por primera vez lo cargue y lo guarde localmente.

-Se valora en negativo el no mostrar información de favorito en la celda de modo que, con un solo golpe de vista localicemos favoritos en la lista.

    Considero que no es necesario porque la seccion de favoritos aparece siempre al comienzo de la lista y en el view del libro hay un botón que indica si es favorito o no.
    Sin embargo para revisar los temas de las celdas personalizadas, lo haré en la práctica de intermedio que también me lo están solicitando.
-Se valora en negativo el que aparezca la sección de favoritos vacía incluso cuando no hay ningún elemento en esta sección.
    
    Esto ya lo hice para la práctica de intermedio.




