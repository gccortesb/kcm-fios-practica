//
//  AGTBook.m
//  HackerBooks
//
//  Created by Gloria Cortés on 23/03/15.
//  Copyright (c) 2015 Gloria Cortés. All rights reserved.
//

#import "AGTBook.h"
#import "KCMDataLoader.h"

@implementation AGTBook


#pragma mark - Init
- (id) initWithTitle: (NSString *) aTitle
             authors: (NSArray *)listAuthors
                tags: (NSArray *) listTags
          anUrlImage: (NSURL *) imageURL
            anPDFUrl:(NSURL *) pdfURL {
    
    if(self = [super init]) {
        
        _title = aTitle;
        _authors =listAuthors;
        _tags = listTags;
        _urlImage = imageURL;
        _urlPdf = pdfURL;
        _favorite = FALSE;
        
    }
    
    return self;
    
}

-(id) initWithDictionary: (NSDictionary *) dict {
    if (self =[super init]) {
        
        NSURL *urlImagen = [NSURL URLWithString:[dict objectForKey:@"image_url"]];
        NSURL *pdfUrl = [NSURL URLWithString: [dict objectForKey:@"pdf_url"]];
        
        
        _title = [dict objectForKey:@"title"];
        _authors = [[dict objectForKey:@"authors"] componentsSeparatedByString:@","];
        _tags=[[dict objectForKey:@"tags"] componentsSeparatedByString:@","];
        _urlImage=urlImagen;
        _urlPdf= pdfUrl;
        
    }
    return self;
}

#pragma mark  - Sobreescritos

-(void) setFavorite: (BOOL) newValue{
    if (newValue != _favorite) {
        _favorite = newValue;
        //Se envia notificación de que se cambio el estado de favorito
        //para que se elimine/incluya el libro en la lista de favoritos
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        NSDictionary *dict = @{BOOK_KEY:self};
        NSNotification *not = [NSNotification notificationWithName:BOOK_DID_CHANGE_FAVORITE_STATE
                                                            object:self userInfo:dict];
        [nc postNotification:not];
    }
}

#pragma mark -utils

-(UIImage *) bookImage {
    
    KCMDataLoader *dataLoader = [[KCMDataLoader alloc] initWithUrlString:self.urlImage.absoluteString];
    [dataLoader loadData];
    UIImage *bookImage = [UIImage imageWithData: dataLoader.data];
    
    return bookImage;
}


- (NSComparisonResult)compare:(AGTBook *)otherBook {
    
    return [self.title compare:otherBook.title];
    
}

@end
