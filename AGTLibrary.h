//
//  AGTLibrary.h
//  HackerBook
//
//  Created by Gloria Cortés on 27/03/15.
//  Copyright (c) 2015 KeepCodingMaster. All rights reserved.
//

@import Foundation;
#import "AGTBook.h"


@interface AGTLibrary : NSObject

@property (nonatomic, strong) NSMutableArray *books;
@property (nonatomic, strong) NSMutableArray *favorites;
@property (nonatomic, strong) NSMutableDictionary *listsByTag;
@property (nonatomic, strong) NSArray *orderedTags;

//Metodos inicializadores
-(id) initWithData:(NSData *) data;


// Metodos de consulta

-(NSUInteger)booksCount;
-(NSArray *) tags;
-(NSUInteger)bookCountForTag: (NSString *) tag;
-(NSArray *) booksForTag:(NSString *) tag;
-(AGTBook *) bookForTag: (NSString *) tag atIndex: (NSUInteger) index;


-(NSUInteger)bookCountFavorites;
-(AGTBook *) bookAtIndex: (NSUInteger) indice;
-(AGTBook *) favoriteAIndex: (NSUInteger) indice;



//Metodos de modificación
-(void)changeFavoriteBooK:(AGTBook*) book;

@end
