//
//  TableViewController.m
//  HackerBook
//
//  Created by Gloria Cortés on 28/03/15.
//  Copyright (c) 2015 KeepCodingMaster. All rights reserved.
//

#import "AGTLibraryTableViewController.h"
#import "AGTBookViewController.h"
#import "AGTLibrary.h"
#import "Settings.h"

@interface AGTLibraryTableViewController ()

@end

@implementation AGTLibraryTableViewController

-(id) initWithModel: (AGTLibrary *) libModel {
    
    if (self = [super initWithStyle:UITableViewStylePlain]) {
        _libraryModel = libModel;
        self.title =@"Biblioteca Personal";
    }
    
    // Se suscribe como observador de la notificación de cambio de estados de favoritos
    // en los libros para poder actualizar el listado de favoritos
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self
           selector: @selector(notifyThatBookFavoriteDidChange:)
               name:BOOK_DID_CHANGE_FAVORITE_STATE
             object:nil];
    
    return self;
    
    
}

- (void) dealloc {
    // Se da de baja en la notificacion de favoritos
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name:BOOK_DID_CHANGE_FAVORITE_STATE object:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Secciones = numero de tags + favoritos
    return [self.libraryModel.tags count] + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number de libros en la librería o los de la lista de favoritos
    
    if (section == FAVORITE_SECTION) {
        return [self.libraryModel bookCountFavorites];
        
    } else {
        
        int s = (int)section - 1;
        
        NSString *tag =[[self.libraryModel tags] objectAtIndex:(NSUInteger)s];
        
        return [self.libraryModel bookCountForTag:tag];
        
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    
    //Recuperar el libro seleccionado
    
    AGTBook *book = [self bookAtIndex:indexPath.row InSection:indexPath.section];
    
    //Se crea la celda
    
    static NSString *cellId = @"LibraryCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (cell == nil) {
        //Se crea una nueva celda porque no hay en el cache de celdas una
        //disponible
        
        cell = [[UITableViewCell alloc]
                 initWithStyle:UITableViewCellStyleSubtitle
                 reuseIdentifier:cellId];
    }
    
    //Sincronizar la celda (vista) con el libro (modelo)
    
    cell.imageView.image = [book bookImage];
    cell.textLabel.text  = book.title;
    cell.detailTextLabel.text = [[book.authors componentsJoinedByString:@", "] stringByAppendingString:@"."];;
    
    
    return cell;
}


-(NSString*) tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section{
    
    if (section == FAVORITE_SECTION) {
        return @"Favoritos";
    } else {
        
        int s = (int)section - 1;
        
        NSString *tag =[[self.libraryModel tags] objectAtIndex:(NSUInteger)s];

        return tag;
    }
}

#pragma mark - Delegate

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //Recuperar el modelo del libro seleccionado
    
    AGTBook *book = [self bookAtIndex:indexPath.row InSection:indexPath.section];
    
    
    //Enviar mensaje al delegado ... solo si implementa el metodo

    if ([self.delegate respondsToSelector:@selector(libraryTableViewController:didSelectBook:)]){
        [self.delegate libraryTableViewController:self didSelectBook:book];
    }
    
    // Enviar notificación para que el visor de pdf cambie el pdf
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    NSNotification *n = [NSNotification notificationWithName:BOOK_DID_CHANGE_NOT
                                                      object:self
                                                    userInfo:@{BOOK_NOT_KEY:book}];
    [nc postNotification:n];
    
    //Guardar el libro seleccionado en el NSUserDefault
    
    NSArray *coords = @[@(indexPath.section),@(indexPath.row)];
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setObject:coords forKey:LAST_SELECTED_BOOK];
    [def synchronize];
    
}

#pragma mark -Notifications
-(void) notifyThatBookFavoriteDidChange :(NSNotification*)notification {
    
    //Sacar el libro
    AGTBook *book = [notification.userInfo objectForKey:BOOK_KEY];
    
    //actualizar el modelo
    [self.libraryModel changeFavoriteBooK:book];
    
    [self.tableView reloadData];

    
}


#pragma mark -utils

-(AGTBook *) bookAtIndex: (NSUInteger) index InSection: (NSUInteger) section {
   if (section == FAVORITE_SECTION) {
        return [self.libraryModel favoriteAIndex:index];
    } else {
        int s = (int)section - 1;
        
        NSString *tag =[[self.libraryModel tags] objectAtIndex:(NSUInteger)s];
        
        return [[self.libraryModel booksForTag:tag] objectAtIndex:index];
    }
}

#pragma mark - s

-(void) libraryTableViewController:(AGTLibraryTableViewController *)lVC didSelectBook:(AGTBook *)book {
    // Se crea el controlador para el libro
    
    AGTBookViewController *bookVC = [[AGTBookViewController alloc] initWithModel:book];
    
    // Se hace un push
    [self.navigationController pushViewController:bookVC animated:YES];
    
}

@end
