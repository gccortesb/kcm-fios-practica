//
//  AGTBookViewController.m
//  HackerBook
//
//  Created by Gloria Cortés on 24/03/15.
//  Copyright (c) 2015 KeepCodingMaster. All rights reserved.
//

#import "AGTBookViewController.h"
#import "AGTSimplePdfViewController.h"
#import "AGTBook.h"

@interface AGTBookViewController ()

@end

@implementation AGTBookViewController

-(id) initWithModel:(AGTBook *)model {
    
    if (self = [super initWithNibName:nil bundle:nil ]) {
        _model = model;
        self.title = model.title;

    }

    return self;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Asegurarse de que no se ocupa toda la pantalla
    // cuando está en un combinador
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    [self syncWithModel];
    
    // Poner el boton de navegación cuando esta en un splitViewController
    self.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - actions

-(IBAction)favorito:(id)sender{
    
    NSLog(@"%d",self.favoriteButton.on);
    self.model.favorite = self.favoriteButton.on;
    
}

-(IBAction) displayPdf:(id)sender {
    
    // Crar el View Controller para el PDF
    
    AGTSimplePDFViewController *pdfVC = [[AGTSimplePDFViewController alloc] initWithModel:self.model];
    
    //Se hace el push en el Navigation Controller
    
    [self.navigationController pushViewController:pdfVC
                                         animated:YES];
    
}



#pragma mark - utils
-(void) syncWithModel{
    
    self.title = self.model.title;
    self.titleLabel.text = self.model.title;
    self.authorsLabel.text =  [[self.model.authors componentsJoinedByString:@", "] stringByAppendingString:@"."];
    self.tagsLabel.text = [[self.model.tags componentsJoinedByString:@", "] stringByAppendingString:@"."];
    
    
    //Recuperar la imagen de la URL
    
    UIImage *bookImage = [self.model bookImage];
    
    self.frontImage.image = bookImage;
    
    [self.favoriteButton setOn:self.model.favorite];
    
}



#pragma mark -UISplitViewControllerDelegate
-(void) splitViewController:(UISplitViewController *)svc willChangeToDisplayMode:(UISplitViewControllerDisplayMode)displayMode {
    if (displayMode == UISplitViewControllerDisplayModePrimaryHidden) {
        //La tabla no se ve y se debe colocar el botón arriba a la
        // izaquierda
        self.navigationItem.leftBarButtonItem = svc.displayModeButtonItem;
    } else {
        //La tabla se ve no se necesita el botón
        self.navigationItem.leftBarButtonItem=nil;
    }
}
#pragma mark -AGTLibraryTableViewControllerDelegate

-(void) libraryTableViewController:(AGTLibraryTableViewController *)lVC didSelectBook:(AGTBook *)book {
    // se actualiza el modelo
    
    self.model = book;
    
    //sincronizar modelo vista
    [self syncWithModel];
}

@end
