//
//  AGTBookViewController.h
//  HackerBook
//
//  Created by Gloria Cortés on 24/03/15.
//  Copyright (c) 2015 KeepCodingMaster. All rights reserved.
//

@import UIKit;
@class AGTBook;

#import "AGTLibraryTableViewController.h"



@interface AGTBookViewController :UIViewController<UISplitViewControllerDelegate, AGTLibraryTableViewControllerDelegate>

@property (nonatomic,strong) AGTBook *model;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorsLabel;
@property (weak, nonatomic) IBOutlet UILabel *tagsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *frontImage;
@property (weak, nonatomic) IBOutlet UISwitch *favoriteButton;


-(id) initWithModel: (AGTBook *) model;
-(IBAction)favorito:(id)sender;
-(IBAction) displayPdf:(id)sender;

@end
