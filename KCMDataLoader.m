//
//  KCMDataLoader.m
//  HackerBook
//
//  Created by GLORIA C CORTES B on 9/07/15.
//  Copyright (c) 2015 KeepCodingMaster. All rights reserved.
//

#import "KCMDataLoader.h"

@implementation KCMDataLoader
-(id) initWithUrlString: (NSString *) urlString {
    
    if (self=[super init]){
        
        _urlString = urlString;
        _data  = [NSData new];
    }
    return self;
    
}
-(void) loadData {
    // Se obtiene una URLLocal para el archivo remoto
    
    NSURL *url = [NSURL URLWithString:self.urlString];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    NSURL *docs = [[fm URLsForDirectory:NSDocumentDirectory
                              inDomains:NSUserDomainMask] lastObject];
    NSString *fileName = [url lastPathComponent];
    NSURL *localFileUrl = [docs URLByAppendingPathComponent:fileName];
    
    //Se usa la data si ya se habia cargado, sino se carga
    
    if ([fm fileExistsAtPath:[localFileUrl path]]) {
        self.data = [NSData dataWithContentsOfURL:localFileUrl];
        
    }else{
        // Se descarga y se guarda sincronicamente
        
        
        self.data = [NSData dataWithContentsOfURL:url];
        
        [self.data writeToURL:localFileUrl
                      atomically:YES];
        
    }
}




@end
